//
//  Number.h
//  CommandLineTool
//
//  Created by Will Fairman on 20/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#ifndef CommandLineTool_Number_h
#define CommandLineTool_Number_h

template <class Type> class Number
{
public:
    Type get() const
    {
        std::cout << "yes value";
        return value;
    }
    void set(Type newValue) {
        value = newValue;
    }
private:
    Type value;
};

#endif
