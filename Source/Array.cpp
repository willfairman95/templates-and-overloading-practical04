//
//  Array.cpp
//  CommandLineTool
//
//  Created by Will Fairman on 09/10/2015.
//  Copyright © 2015 Tom Mitchell. All rights reserved.
//

#include "Array.hpp"

/** Class for creating a dynamic array*/

Array::Array()
{
    size = 0;
    pfloat = new float[size];
}
Array::~Array()
{
    //delete memory allocated to array
    delete [] pfloat;
}
void Array::add(float itemValue)
{
    //create temp array to store new values
    float* ptemp = new float[size+1];
    
    //store new values on pfloat
    for (int i = 0; i<=size; i++)
    {
        ptemp[i] = pfloat[i];
    }
    ptemp[size+1] = itemValue;
    delete [] pfloat;
    pfloat = ptemp;
    size++;
}
float Array::get(int index) const
{
    //return item at index
    if (index >= 0 && index < size) {
        return pfloat[index+1];
    }
    else
        return 0.f;
}
int Array::getSize()
{
    //returns number of items currently in the array
    return size;
}