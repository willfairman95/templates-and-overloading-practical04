//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <stdio.h>
#include <iostream>

#include "Array.hpp"
#include "Number.h"

//function declerations
//min
template <typename Type>
Type min(Type a, Type b);

//add
template <typename Type>
Type add(Type a, Type b);

//print
template <typename Type, size_t size>
void print(const Type (&array)[size]);

//getAverage
template <typename Type, size_t size>
void getAverage(const Type (&array)[size]);

//create objects of type "Number"
Number<float> numf;
Number<int> numi;
Number<std::string> numstr;

int main (int argc, const char* argv[])
{
    //min
    //std::cout << "Minimum value = " << min(10, 20) << std::endl;
    
    //add
    std::cout << "Sum = " << add(100, 50) << std::endl;
    
    //print
    int array[5] = {21, 42, 67, 84, 105};
    print(array);
    
    //getAverage
    getAverage(array);
    
    //add values to number objects and print them
    numf.set(0.5f);
    numi.set(2);
    numstr.set("yess");
    
    std::cout << numf.get() << "\n"; std::cout << numi.get() << "\n"; std::cout << numstr.get() << "\n";
    
    return 0;
}

//template function definitions

//template function that returns the minimum of two values
template <typename Type>
Type min (Type a, Type b)
{
    
    if (a > b)
        return b;
    else
        return a;
    
}

//template function at returns the sum of its two arguments
template <typename Type>
Type add (Type a, Type b)
{
    Type c = a + b;
    
    return c;
}

//template function that takes an array of a generic type and an int specifying the size of the array. Then
//pritns every item in the array

template <typename Type, size_t size>
void print (const Type (&array)[size])
{
    std::cout << "Array items:" << std::endl;
    
    for(int i = 0; i < size; ++i)
        std::cout << array[i] << " " << std::endl;
}

//template function that takes an array of a generic type and an in specifying the size of array, then calculates the mean average and returns this value as double

template <typename Type, size_t size>
void getAverage(const Type (&array)[size])
{
    float sum;
    double average;
    
    for (int i : array)
        sum += i;
    
    average = sum / size;
    
    std::cout << "Average: " << average << std::endl;
}









//
//bool testArray()
//{
//    Array array;
//    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
//    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
//    
//    if (array.getSize() != 0)
//    {
//        std::cout << "size is incorrect\n";
//        return false;
//    }
//    
//    for (int i = 0; i < testArraySize; i++)
//    {
//        array.add (testArray[i]);
//        
//        if (array.getSize() != i + 1)
//        {
//            std::cout << "size is incorrect\n";
//            return false;
//        }
//        
//        if (array.get (i) != testArray[i])
//        {
//            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
//            return false;
//        }
//    }

    //    //removing first
    //    array.remove (0);
    //    if (array.size() != testArraySize - 1)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        if (array.get(i) != testArray[i+1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //removing last
    //    array.remove (array.size() - 1);
    //    if (array.size() != testArraySize - 2)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        if (array.get(i) != testArray[i + 1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //remove second item
    //    array.remove (1);
    //    if (array.size() != testArraySize - 3)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    if (array.get (0) != testArray[1])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //
    //    if (array.get (1) != testArray[3])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //    
    //    if (array.get (2) != testArray[4])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    
//    return true;
//}