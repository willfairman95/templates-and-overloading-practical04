//
//  LinkedList.cpp
//  CommandLineTool
//
//  Created by Will Fairman on 09/10/2015.
//  Copyright © 2015 Tom Mitchell. All rights reserved.
//

#include "LinkedList.hpp"

/* Class for managing a linked list of floats **/

LinkedList::LinkedList()
{
    
    head = nullptr;
}
LinkedList::~LinkedList()
{
    while (head != nullptr) {
        Node* temp = head;
        head = head->next;
        delete temp;
    }
    
}
void LinkedList::add(float itemValue)
{
    Node* newNode = new Node;
    newNode->value = itemValue;
    newNode->next = nullptr;
    
    Node* temp = head;
    while (temp != nullptr) {
        temp = temp->next;
    }
    temp->next = newNode;
    
}
float LinkedList::get(int index)
{
    int count = 0;
    Node* temp = head;
    while (count < index) {
        temp = temp->next;
    }
    return temp->value;
}
int LinkedList::getSize()
{
    int count = 0;
    Node* temp = head;
    while (temp->next != nullptr) {
        temp = temp->next;
        count++;
    }
    return count;
}