//
//  LinkedList.hpp
//  CommandLineTool
//
//  Created by Will Fairman on 09/10/2015.
//  Copyright © 2015 Tom Mitchell. All rights reserved.
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <stdio.h>

/* Class for managing a linked list of floats **/

class LinkedList
{
public:
    
    //constructor
    LinkedList();
    
    //deconstructor
    ~LinkedList();
    
    //add new items to the end of array
    void add (float itemValue);
    
    //returns value stored in the node at specified index
    float get (int index);
    
    //returns the number of items currently in the linked list
    int getSize();
    
private:
    struct Node
    {
        float value;
        Node* next;
    };
    Node* head;
};

#endif /* LinkedList_hpp */
