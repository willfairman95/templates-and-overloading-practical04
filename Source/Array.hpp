//
//  Array.hpp
//  CommandLineTool
//
//  Created by Will Fairman on 09/10/2015.
//  Copyright © 2015 Tom Mitchell. All rights reserved.
//

/** Class for creating a dynamic array*/


#define Array_hpp

#include <stdio.h>

using namespace std;

template<class T>
class Array
{
public:
    
    typedef T element_type;
    
    //constructor
    Array();
    
    //deconstructor
    ~Array();
    
    //add new items to end of array
    void add (element_type itemValue);
    
    //return the item at the index
    element_type get (int index) const;
    
    //return the number of items currently in array
    int getSize();
    
private:
    int size;
    element_type* pfloat;
};

template<class T>
Array<T>::Array()
{
    size = 0;
    pfloat = new element_type[size];
}

template<class T>
Array<T>::~Array()
{
    //delete memory allocated to array
    delete [] pfloat;
}

template<class T>
void Array<T>::add(element_type itemValue)
{
    //create temp array to store new values
    element_type* ptemp = new float[size+1];
    
    //store new values on pfloat
    for (int i = 0; i <= size; i++) {
        ptemp[i] = pfloat[i];
    }
    ptemp[size+1] = itemValue;
    delete [] pfloat;
    pfloat = ptemp;
    size++;
}

template<class T>
typename Array<T>::element_type Array<T>::get (int index) const
{
    //return item at index
    if (index >= 0 && index < size){
        return pfloat [index+1];
    }
    else
        return element_type();
}
template<class T>
int Array<T>::getSize()
{
    //returns number of items currently in the array
    return size;
}

